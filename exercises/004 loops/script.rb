# for loop
for i in 1...5 # 1 to 4
    puts i
end

for i in 1..5 # 1 to 5
    puts i
end

i = 1
loop do
    i += 1
    next if i % 2 = 1 # skip if i is odd number
    puts i
    break if i == 5
end

i = 1
while i <= 5 do
    puts i
    i += 1
end

